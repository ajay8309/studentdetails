package com.web;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.dao.StudentDAO;
import com.dto.Student;


@WebServlet("/GetAllStd")
public class GetAllStd extends HttpServlet {
	
protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		response.setContentType("text/html");
		PrintWriter out = response.getWriter();
		
		StudentDAO StdDao = new StudentDAO();		
		List<Student> StdList = StdDao.getAllStudents();
		
		RequestDispatcher rd = request.getRequestDispatcher("TCHomePage");
		rd.include(request, response);
		
		out.println("<center>");
		
		if (StdList != null) {
			
			out.println("<table border=2>");
			
			out.println("<tr>");
			out.println("<th>stdId</th>");
			out.println("<th>stdName</th>");
			out.println("<th>Batch</th>");
			out.println("<th>Gender</th>");
			out.println("<th>Email-Id</th>");
			out.println("</tr>");
			
			for (Student std : StdList) {
				out.println("<tr>");
				out.println("<td>" + std.getstdId()   + "</td>");
				out.println("<td>" + std.getstdName() + "</td>");
				out.println("<td>" + std.getBatch()  + "</td>");
				out.println("<td>" + std.getGender()  + "</td>");
				out.println("<td>" + std.getEmailId() + "</td>");
				out.println("</tr>");
			}
			
			out.println("</table>");			
		
		} else {			
			out.println("<h1 style='color:red;'>Unable to Fetch the Student Record(s)!!!</h1>");	
		}
		out.println("</center>");
	}
    
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		doGet(request, response);
	}

}